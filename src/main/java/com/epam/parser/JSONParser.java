package com.epam.parser;

import com.epam.model.Candy;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;

public class JSONParser {
    private static Logger logger = LogManager.getLogger(JSONParser.class);

    public static void main(String[] args) {
        ResourceBundle bundle = ResourceBundle.getBundle("config");
        File json = new File(bundle.getString("CandyJSON"));
        File jsonScheme = new File(bundle.getString("CandyJSONScheme"));
        parse(json, jsonScheme);
    }

    private static void parse(File json, File jsonScheme) {
        try {
            if (JSONValidator.validateJSON(json, jsonScheme)) {
                printList(JSONSimpleParser.getCandyList(json),"JSON Simple Parser");
                printList(Arrays.asList(new Gson().fromJson(new FileReader(json), Candy[].class)),
                        "JSON GSON Parser:");
                printList(new ObjectMapper().readValue(json, new TypeReference<List<Candy>>() {}),
                        "JSON Jackson Parser:");
            } else {
                logger.info("Your JSON file is not valid!\n");
            }
        } catch (IOException e) {
            logger.info("IOException");
        } catch (ProcessingException e) {
            logger.info("ProcessingException");
        }

    }

    private static void printList(List<Candy> candies, String parserName) {
        candies.sort(Comparator.comparing(Candy::getEnergy));
        logger.info(parserName + "\n");
        for (int i = 0; i < candies.size(); i++) {
            logger.info((i + 1) + "." + candies.get(i) + "\n");
        }
    }
}
