package com.epam.parser;

import com.epam.model.Candy;
import com.epam.model.Ingredient;
import com.epam.model.Value;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class JSONSimpleParser {
    private static Logger logger = LogManager.getLogger(JSONSimpleParser.class);

    public static List<Candy> getCandyList(File json) {
        JSONParser parser = new JSONParser();
        List<Candy> candies = new ArrayList<>();
        try {
            Object obj = parser.parse(new FileReader(json));
            JSONArray jsonArray = (JSONArray) obj;
            for (Object o : jsonArray) {
                Candy candy = new Candy();
                setFields(candy, (JSONObject) o);
                candies.add(candy);
            }
        } catch (FileNotFoundException e) {
            logger.info("FileNotFoundException");
        } catch (IOException e) {
            logger.info("IOException");
        } catch (ParseException e) {
            logger.info("ParseException");
        }
        return candies;
    }

    private static void setFields(Candy candy, JSONObject jsonObject) {
        candy.setType((String) jsonObject.get("type"));
        candy.setName((String) jsonObject.get("name"));
        candy.setEnergy(Integer.valueOf(String.valueOf(jsonObject.get("energy"))));
        candy.setProduction((String) jsonObject.get("production"));

        JSONObject values = (JSONObject) jsonObject.get("values");
        candy.setValues(new Value(Double.valueOf(values.get("proteins").toString()),
                Double.valueOf(values.get("carbohydrates").toString()),
                Double.valueOf(values.get("fats").toString())));
        JSONObject ingredients = (JSONObject) jsonObject.get("ingredients");
        Ingredient ingredient = new Ingredient();
        ingredient.setWater(Double.valueOf(ingredients.get("water").toString()));
        ingredient.setVanilin(Double.valueOf(ingredients.get("vanilin").toString()));
        ingredient.setFructose(Double.valueOf(ingredients.get("fructose").toString()));
        ingredient.setSugar(Double.valueOf(ingredients.get("sugar").toString()));
        if(ingredients.containsKey("chocolateType")){
            ingredient.setChocolateType(ingredients.get("chocolateType").toString());
        }
        candy.setIngridients(ingredient);
    }
}
