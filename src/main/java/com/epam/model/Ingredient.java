package com.epam.model;

public class Ingredient {
    private double water;
    private double sugar;
    private double fructose;
    private double vanilin;
    private String chocolateType;

    public Ingredient(){}

    public Ingredient(double water, double sugar, double fructose, double vanilin,
                       String type) {
        this.fructose = fructose;
        this.sugar = sugar;
        this.vanilin = vanilin;
        this.water = water;
        chocolateType = type;
    }

    public void setAll(double water, double sugar, double fructose, double vanilin,
                      String type) {
        this.fructose = fructose;
        this.sugar = sugar;
        this.vanilin = vanilin;
        this.water = water;
        chocolateType = type;
    }

    public String getChocolateType() {
        return chocolateType;
    }

    public void setChocolateType(String chocolateType) {
        this.chocolateType = chocolateType;
    }

    public double getWater() {
        return water;
    }

    public void setWater(double water) {
        this.water = water;
    }

    public double getSugar() {
        return sugar;
    }

    public void setSugar(double sugar) {
        this.sugar = sugar;
    }

    public double getFructose() {
        return fructose;
    }

    public void setFructose(double fructose) {
        this.fructose = fructose;
    }

    public double getVanilin() {
        return vanilin;
    }

    public void setVanilin(double vanilin) {
        this.vanilin = vanilin;
    }

    @Override
    public String toString() {
        return "{ Water=" + water + " milligram, Sugar=" + sugar
                + " milligram, Chocolate type=" + chocolateType
                + ", Fructose=" + fructose + " milligram, Vanilin="
                + vanilin + " milligram}\n";
    }
}
